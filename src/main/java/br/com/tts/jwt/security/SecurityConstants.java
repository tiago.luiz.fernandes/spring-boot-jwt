package br.com.tts.jwt.security;

import java.util.concurrent.TimeUnit;


public class SecurityConstants {
    public static final String SECRET = "SEGREDO";
    public static final long EXPIRATION_TIME = TimeUnit.DAYS.toMillis(1);
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIG_UP_URL = "/api/v1/authentication/**";
    public static final String STATUS_URL = "/api/v1/status/**";
}
