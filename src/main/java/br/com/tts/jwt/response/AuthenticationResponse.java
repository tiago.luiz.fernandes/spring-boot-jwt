package br.com.tts.jwt.response;

import lombok.Builder;
import lombok.Data;

/**
 * @author Tiago Luiz Fernandes
 */

@Data
@Builder
public class AuthenticationResponse {
    private String token;
}

