package br.com.tts.jwt.controller;

import br.com.tts.jwt.response.AuthenticationResponse;
import br.com.tts.jwt.security.SecurityConstants;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/authentication")
public class AuthenticationController {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostMapping(path = "{username}")
    public ResponseEntity<AuthenticationResponse> login(@PathVariable("username") String username){

        Algorithm algorithm = Algorithm.HMAC512(SecurityConstants.SECRET);

        String token = JWT.create()
                .withClaim("username",username)
                .withExpiresAt(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .sign(algorithm);

        return ResponseEntity.ok(AuthenticationResponse.builder().token(token).build());

    }

}
