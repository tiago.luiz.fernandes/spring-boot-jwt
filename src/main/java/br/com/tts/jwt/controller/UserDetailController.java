package br.com.tts.jwt.controller;

import br.com.tts.jwt.data.UserDetailDTO;
import br.com.tts.jwt.service.UserDetailServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/users")
public class UserDetailController {

    private final UserDetailServiceImpl userDetailService;

    @RequestMapping()
    public List<UserDetailDTO> findAll(){
        return userDetailService.findAll();
    }

}
