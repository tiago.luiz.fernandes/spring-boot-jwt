package br.com.tts.jwt.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/status")
public class StatusController {

    @RequestMapping()
    public String viewStatus(){
        return "online";
    }
}
