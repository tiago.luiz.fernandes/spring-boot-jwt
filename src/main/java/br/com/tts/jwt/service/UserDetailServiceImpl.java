package br.com.tts.jwt.service;

import br.com.tts.jwt.data.UserDetailDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserDetailDTO user = findByUsername(username);

        if (user == null){
            throw new UsernameNotFoundException(username);
        }

        return new User(user.getUsername(),user.getPassword(), Collections.emptyList());
    }

    public UserDetailDTO findByUsername(String username) {
        UserDetailDTO user = new UserDetailDTO();
        user.setUsername("admin");
        user.setPassword(bCryptPasswordEncoder.encode("adm"));
        return user;
    }

    public List<UserDetailDTO> findAll(){
        List<UserDetailDTO> userList = new ArrayList<>();
        userList.add(findByUsername("admin"));
        return userList;
    }

}
